import React from 'react';
import './App.css';
import UsersTable from './components/UsersTable';
import UseUsers from './hooks/UseUsers';

const App = () => {
	const {
		selectedUsers,
		setSelectedUsers,
		fullName,
		setFullName,
		dateFrom,
		setDateFrom,
		dateTo,
		setDateTo,
		profileRole,
		setProfileRole,
		profiles,
		handleFilterUsers,
	} = UseUsers();

	return (
		<div className='page-container'>
			<header>
				<h1>Users List</h1>
				<form
					onSubmit={e => {
						e.preventDefault();
						handleFilterUsers();
					}}
				>
					<div id='filters'>
						<div className='item'>
							<label>Full Name</label>
							<input
								type='text'
								value={fullName}
								id='fullname'
								name='fullname'
								onChange={(e: any) => setFullName(e.target.value)}
							/>
						</div>
						<div className='item'>
							<label>Birtday From</label>
							<input
								type='date'
								value={dateFrom || ''}
								onChange={(e: any) => setDateFrom(e.target.value)}
								id='fullname'
								name='fullname'
							/>
						</div>
						<div className='item'>
							<label>Birthday To</label>
							<input
								type='date'
								value={dateTo || ''}
								onChange={(e: any) => setDateTo(e.target.value)}
								id='fullname'
								name='fullname'
							/>
						</div>
						<div className='item'>
							<label>Profile</label>
							<select
								value={profileRole}
								id='profile'
								name='profile'
								onChange={(e: any) => setProfileRole(e.target.value)}
							>
								{profiles}
							</select>
						</div>
						<div className='item'>
							<button type='submit' value='Submit'>
								Submit
							</button>
						</div>
					</div>
					<UsersTable users={selectedUsers} />
				</form>
			</header>
		</div>
	);
};

export default App;
