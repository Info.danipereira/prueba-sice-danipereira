import React, { useState, useEffect } from 'react';
import UserType from '../types/UserType';
import getUsers from '../services/UserService';
import { Profile, profileValues } from '../types/ProfileEnum';

const UseUsers = () => {
	const [allUsers, setAllUsers] = useState([] as UserType[]);
	const [selectedUsers, setSelectedUsers] = useState([] as UserType[]);
	const [fullName, setFullName] = useState('');
	const [dateFrom, setDateFrom] = useState();
	const [dateTo, setDateTo] = useState();
	const [profileRole, setProfileRole] = useState('');

	useEffect(() => {
		const handleUsers = async () => {
			const data = await getUsers();

			setAllUsers(data);
		};
		handleUsers();
	}, []);

	let profiles = profileValues.map((profile: Profile, j) => {
		return (
			<option key={j} value=''>
				{profile}
			</option>
		);
	});
	profiles.push(<option key={profiles.toString()} value=''></option>);

	const handleFilterUsers = () => {
		let filtered = allUsers;

		if (profileRole) {
			filtered = allUsers.filter(u => u.profile === profileRole);
		}

		if (fullName) {
			filtered = filtered.filter(u =>
				u.fullName.toLowerCase().includes(fullName.toLowerCase())
			);
		}

		if (dateFrom) {
			filtered = filtered.filter(u => u.birthday >= new Date(dateFrom));
		}

		if (dateTo) {
			filtered = filtered.filter(u => u.birthday <= new Date(dateTo));
		}

		setSelectedUsers(filtered);
	};

	return {
		selectedUsers,
		setSelectedUsers,
		fullName,
		setFullName,
		dateFrom,
		setDateFrom,
		dateTo,
		setDateTo,
		profileRole,
		setProfileRole,
		profiles,
		handleFilterUsers,
	};
};

export default UseUsers;
