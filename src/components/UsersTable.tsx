import React from 'react';
import UserType from '../types/UserType';

interface Props {
	users: UserType[];
}

const UsersTable: React.FC<Props> = props => {
	return (
		<div className='UsersTable'>
			<table>
				<thead>
					<tr>
						<th>Full Name</th>
						<th>Birthday</th>
						<th>Profile</th>
					</tr>
				</thead>
				<tbody>
					{props.users?.map((u, index) => (
						<tr key={u.id}>
							<td>{u.fullName}</td>
							<td>{u.birthday.toLocaleDateString('EN')}</td>
							<td>{u.profile}</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>
	);
};

export default UsersTable;
